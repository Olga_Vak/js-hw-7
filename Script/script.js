function createList(arr) {
    let array = arr.map(function(element,i,arr){
        return `<li>${arr[i]}</li>`
    });
    array = array.join('');
    let list = document.createElement('ul');
    list.innerHTML = array;
    document.body.appendChild(list);
}

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
createList(['1', '2', '3', 'sea', 'user', 23]);